---
layout: markdown_page
title: "Francis Potter's README"
---

# Francis Potter
# Senior Professional Services Engineer

---

GitLab has developed an extraordinary product, and I love working with it!

Before joining GitLab, I ran a tiny independent professional services firm, providing application development and strategic services businesses and nonprofits. My 14-year-old son told me of GitLab in 2018, and I immediately converted all my client projects to the platform and remained an advocate and enthusiast ever since.

Outside of this amazing organization, I consider myself an open source Python developer, brain tumour survivor, aspie, Rotarian, gender non-conformist, LGBTQ2S+ ally, concertgoer, musical theatre nut, electric guitar dabbler, MotoGP follower, Rush fan, husband, father, Canadian, and resident of the traditional territory of the K'ómoks people.

Check out my quirky collection of free tools and utilities for devops and personal effectiveness, [Steampunk Wizard](https://gitlab.com/steampunk-wizard).

### CI/CD explainer videos

In my earlier role as a Senior Solution Architect, I developed an informal specialty in GitLab CI/CD configuration and dove deep into some of the features that the product team added during that period. Some of the explainer videos might continue to help with understanding today (although the product changes regularly so please confirm).

- [Merge Trains!](youtube.com/watch?v=X7f51pF3tfU) including details on when pipelines are run and how to discern different conditions within the CI/CD configuration
- [GitLab CI/CD Rules](https://www.youtube.com/watch?v=QjQc-zeL16Q&list=PLsxxpZZ4jDu7NE9azj-yNwGT23zzb0Utu&index=10) from around the time we started retiring the old "only/except" approach
- [Custom Pipeline Form for GitLab CI/CD inside a GitLab Pages site](https://www.youtube.com/watch?v=7NsyCYmVQKw) shows how to design forms on static web sites to drive pipelines, but uses the OAuth2 "implicit grant flow" approach, which is deprecated (or removed?) now
- [Nested dynamic pipelines](https://www.youtube.com/watch?v=C5j3ju9je2M) leads me to think about ways to use the CI/CD platform for training ML models
- [Multi-project pipelines](youtube.com/watch?v=g_PIwBM1J84&list=PLsxxpZZ4jDu7NE9azj-yNwGT23zzb0Utu&index=9)
- [GitLab for machine learning on kubernetes](https://www.youtube.com/watch?v=YiD9Ozj5zbQ&list=PLsxxpZZ4jDu7NE9azj-yNwGT23zzb0Utu&index=6)
- [Overview of GitLab CI/CD including security scanning and deploy boards](https://www.youtube.com/watch?v=wsbSvLyC2Z8&list=PLsxxpZZ4jDu7NE9azj-yNwGT23zzb0Utu&index=5)

### Structured work style

[Busy](https://pypi.org/project/busy/) serves as my daily time and task management platform. When a question comes up that might require some research, I often put it into Busy and come back to it. That way, I control my work rather than allowing interrupts to control my time.

If you need to meet with me today or tomorrow, please Slack me before adding last-minute entries to my calendar. I plan all my time a day in advance, intentionally filling Google Calendar with blocks. 

