---
layout: handbook-page-toc
title: Working on a promotion
category: Support Team
description: Process for submitting a promotions request
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This page supplements the [Promotions and Transfers handbook page](/handbook/people-group/promotions-transfers/)
and documents promotions-related workflows and processes specific to the
Customer Support department.

## Process

The general process for promotions is:

1. Support team member completes promotion document in collaboration with their
   manager.
   - Collaboration with direct peers can also be a great resource in moving a
     promotion document forward. A lot of team members find recognizing and
     praising their co-workers easier than doing so for themselves.
1. The manager circulates the promotion document for review and feedback
   amongst the manager's peers.
1. Support Directs review and calibrate all proposed promotions once a quarter.
1. Approved promotions proceed through the rest of the process.

## Promotion Document

The promotion document template can be found in the
[Pillars section of the Promotions and Transfers handbook page](/handbook/people-group/promotions-transfers/#pillars).

Business Results and Business Justifications section should closely align with
the expected competencies for the role as laid out in:

- The [GitLab Job Frameworks](/company/team/structure/#job-frameworks).
- The [Support Career Framework](/handbook/engineering/career-development/matrix/engineering/support/).

## In-Progress Promotion Documents

Collaboration on promotion documents encourages transparency in the promotion process. It can 
motivate and inspire others to build and strengthen their promotion documents. Internally 
public promotion documents are part of GitLab's  
[Promotion Philosophy](/handbook/people-group/promotions-transfers/#promotion-philosophy). 
GitLab Support team members can link their internally 
public promotion documents in this section. Having a public in-progress promotion document is **optional**. It is encouraged for Staff+ promotions.

If you want to list your document here, decide what level of publicity you are comfortable with. Set the appropriate access for the GitLab group in the document's _Share_ settings: You can keep it read-only (`Viewer`), allow others to comment and suggest edits (`Commenter`) – or fully open it to collaboration (`Editor`).

### Promotion to Senior Support Engineer

 - [David Coy](https://docs.google.com/document/d/182Ijxv5VJjGoDXQFBi2DmFpMdZio324UXMKh7sz2xoo/edit)

### Promotion to Staff Support Engineer

 - [Brie Carranza](https://docs.google.com/document/d/14DbQH4VjbE47r9zefK0HPBKW_ZAfI8Sm_Kqdo_6Svbo/edit)
 - [Manuel Grabowski](https://docs.google.com/document/d/1hGH6ScWyJwVnR5tU-7xG7_WY_ykyyLuzHD-5TJzGme0/edit)