title: British Geological Survey
canonical_path: /customers/bgs/
cover_image: /images/blogimages/bgs-cover.jpg
cover_title: |
  How British Geological Survey revolutionized its software development lifecycle
cover_description: |
  GitLab SCM and CI/CD removes the barriers to software development and allows BGS to accelerate innovative science research.
twitter_image: /images/opengraph/Case-Studies/case-study-bgs.png
twitter_text: Learn how @BritGeoSurvey accelerates innovative science research with @GitLab.
customer_logo: /images/case_study_logos/bgs-logo-reverse.png
customer_logo_css_class: brand-logo-tall
customer_industry: Science and Research
customer_location: Main offices in Nottingham and Edinburgh
customer_solution: GitLab Self-Managed Ultimate
customer_employees: 600 
customer_overview: |
  GitLab provides BGS with an intuitive, single platform for improved efficiency, source code management, and CI/CD.
customer_challenge: |
  BGS needed a tool that would accelerate collaboration between scientists and developers to transform their projects and digital processes.
key_benefits:


    Efficiency improvement 


    End-to-end visibility


    Organizational-wide collaboration


    Flexible hosting options


    Built in security and compliance


    Kubernetes integration


    Easy user interface 
customer_stats:
  - stat: 200  
    label: Users on the platform (including developers, scientists and stakeholders)
  - stat: |
      1,000      
    label: Hosted software projects and components 
  - stat: +140k     
    label: Automated pipelines jobs have been run to integrate, test and deploy scientific projects
customer_study_content:
  - title: the customer
    subtitle: Geosciences research organization
    content: >-
    

        The British Geological Survey is a 185 year old science and educational research organization that focuses on the geological properties of the UK. BGS’s clients include the public, scientific organizations, universities, and the government in the UK and abroad. The [BGS](https://www.bgs.ac.uk/home.html) provides expert advice in all aspects of geosciences and helps society to use natural resources responsibly, manage environmental change, and be resilient to environmental hazards. The organization digitally records their findings and research.
    
  - title: the challenge
    subtitle: Lacking transparency and collaboration
    content: >-
    

        With about 600 employees, two thirds are scientists and the other third are made up of business support and informatics teams. Communication between scientists and developers is key to ensuring that environmental projects move forward and that information relating to their interactions is recorded.   
    

        Previously software developers were using [Subversion for source code management](/customers/axway-devops/), but found the tool to be lacking the visibility the teams required. "With Subversion we were storing our code in a secure and convenient place, but it didn't have the collaboration aspect that GitLab offers. You couldn't see the visibility of the work that you were doing and you couldn't share that with our scientists, stakeholders and managers interested in that project," explained Wayne Shelley, DevOps integration leader, BGS.
    

        Without a singular tool that offered total visibility, the BGS software development lifecycle suffered. The teams were not using any CI/CD and processes were separated into siloed steps. "If we wanted to run tests, we'd probably be doing them offline or we probably wouldn't be doing them at all! We certainly didn't have any kind of continuous integration and deployment that we have now." Shelley said.
    

        BGS has its own infrastructure and with developers' demand for infrastructure on the rise, DevOps practices have helped to bridge the gap between infrastructure and application development. "We’re not a software development house, so this kind of platform infrastructure was lacking at our organization," Shelley added. 
    

        BGS needed an all-in-one tool that offered an easy to use interface, enhanced collaboration, complete visibility, and seamless integration. As Shelley put it, they wanted "an off-the-shelf, all-in-one solution that you know we could get behind."
   
  - title: the solution
    subtitle: An all-in-one, self-managed platform
    content: >-
    

        About six years ago, Wayne was working on some personal software development projects at home and discovered GitLab. For him, GitLab stood out over GitHub for its ability to be self-managed. After using it personally for a few weeks, Shelley installed it at work and integrated it into some of the existing systems. From there, GitLab’s adoption spread throughout the organization quickly and organically.
    

        One of the reasons that GitLab worked so well for BGS is that the teams needed their code and infrastructure to remain secure, not necessarily open to the public. Having GitLab locally meant that they could have private repositories on a self-managed infrastructure.
    

        Shelley and his team also like that the [project management capabilities](/topics/version-control/) of GitLab integrate easily with the web interface. “It’s an excellent way of exploring your code. You can see your projects and be proud of what you're creating. You can see the changes you're making and so can everyone else in the organization. It is discoverable and more interesting. But most importantly our scientific staff are much more involved in the software development process and this accelerated our delivery and research capabilities,” Shelley said.

    
  - title: the results
    subtitle: Accelerated software development workflow
    content: >-
    

        Over the last few years GitLab has provided a fast track to an improved software development lifecycle. “When we moved to GitLab, it brought to the forefront all of the best practices that the industry is using. We're a relatively small set of developers, so for someone to have packaged up all the best practices into a nice platform is awesome,” Shelley said.
    

        Developers now write their own code, create builds, run tests, and develop source-controlled projects all in GitLab. There is now the ability for software developers to collaborate on various projects because of the level of transparency and accessibility within the platform. “The whole process of developing a piece of software has accelerated over the last five years. And I think GitLab has been a focal point of all of the infrastructure and tools and systems that we've built up around it,” according to Shelley.    
    

        BGS has created a process that is collaborative, code is visible to everyone, work is showcased, security testing is built-in, and deployments have accelerated. According to Shelley, “GitLab has really revolutionized and professionalized the whole software development lifecycle in our organization.” GitLab has provided a workflow, and improved BGS best practices in terms of how they’re doing software development. 
    

        GitLab Self-Managed Ultimate has a huge range of additional features that is now empowering the software development teams at BGS. But most importantly, “Knowing that GitLab Support is there for you and has your back if you need them. That's critical,” Shelley concluded.  
    

    
      
    

        ## Learn more about Self-Managed Ultimate
    

        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
    

        [The benefits of GitLab CI](/features/continuous-integration/)
    

        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: GitLab has become our central place to store code and issues. It's become a mission critical system for our organization.
    attribution: Wayne Shelley  
    attribution_title: DevOps integration leader, BGS 
  - blockquote: By using GitLab because we're developing better solutions and we're able to deploy them quicker. We are more confident in the work and it has definitely improved our efficiency.
    attribution: Wayne Shelley  
    attribution_title: DevOps integration leader, BGS

