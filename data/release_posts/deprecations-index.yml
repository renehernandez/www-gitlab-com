---
'16.1':
- Deprecate Windows CMD in GitLab Runner
- GraphQL deprecation of dependencyProxyTotalSizeInBytes field
- Running a single database is deprecated
- Unified approval rules are deprecated
'16.0':
- CiRunner.projects default sort is changing to id_desc
- CiRunnerUpgradeStatusType GraphQL type renamed to CiRunnerUpgradeStatus
- GraphQL type, RunnerMembershipFilter renamed to CiRunnerMembershipFilter
- PostgreSQL 13 deprecated
- sidekiq delivery method for incoming_email and service_desk_email is deprecated
- Adding non-LDAP synced members to a locked LDAP group is deprecated
- Bundled Grafana deprecated and disabled
- Changing MobSF-based SAST analyzer behavior in multi-module Android projects
'15.9':
- Accessibility Testing is deprecated
- Browser Performance Testing is deprecated
- Filepath field in Releases and Release Links APIs
- GitLab Runner platforms and setup instructions in GraphQL API
- Load Performance Testing is deprecated
- Queue selector for running Sidekiq is deprecated
- Required Pipeline Configuration is deprecated
- Single database connection is deprecated
- Slack notifications integration
- The GitLab legacy requirement IID is deprecated in favor of work item IID
- Trigger jobs can mirror downstream pipeline status exactly
- HashiCorp Vault integration will no longer use CI_JOB_JWT by default
- Old versions of JSON web tokens are deprecated
- License Compliance CI Template
- CI/CD jobs will fail when no secret is returned from Hashicorp Vault
- Default CI/CD job token (CI_JOB_TOKEN) scope changed
- Deprecation and planned removal for CI_PRE_CLONE_SCRIPT variable on GitLab SaaS
- Development dependencies reported for PHP and Python
- Embedding Grafana panels in Markdown is deprecated
- Enforced validation of CI/CD parameter character lengths
- External field in GraphQL ReleaseAssetLink type
- External field in Releases and Release Links APIs
- Legacy Praefect configuration method
- Legacy URLs replaced or removed
- License-Check and the Policies tab on the License Compliance page
- Managed Licenses API
- Option to delete projects immediately is deprecated from deletion protection settings
- SAST analyzer coverage changing in GitLab 16.0
- Secure analyzers major version update
- Secure scanning CI/CD templates will use new job rules
- Support for Praefect custom metrics endpoint configuration
'15.8':
- Auto DevOps support for Herokuish is deprecated
- GitLab Helm chart values gitlab.kas.privateApi.* are deprecated
- 'GraphQL: The DISABLED_WITH_OVERRIDE value of the SharedRunnersSetting enum is deprecated.
  Use DISABLED_AND_OVERRIDABLE instead'
- Maintainer role providing the ability to change Package settings using GraphQL API
- The Visual Reviews tool is deprecated
- Auto DevOps no longer provisions a PostgreSQL database by default
- Azure Storage Driver defaults to the correct root prefix
- Conan project-level search endpoint returns project-specific results
- Configuring Redis config file paths using environment variables is deprecated
- Container Registry pull-through cache
- Cookie authorization in the GitLab for Jira Cloud app
- Dependency Scanning support for Java 13, 14, 15, and 16
- Deployment API returns error when updated_at and updated_at are not used together
- Developer role providing the ability to import projects to a group
- GitLab.com importer
- Limit personal access token and deploy token’s access with external authorization
- Non-standard default Redis ports are deprecated
- Projects API field operations_access_level is deprecated
- Rake task for importing bare repositories
- The API no longer returns revoked tokens for the agent for Kubernetes
- The latest Terraform templates will overwrite current stable templates
- Use of third party container registries is deprecated
- environment_tier parameter for DORA API
- openSUSE Leap 15.3 packages
- Automatic backup upload using Openstack Swift and Rackspace APIs
- Live Preview no longer available in the Web IDE
'15.7':
- DAST ZAP advanced configuration variables deprecation
- Support for REST API endpoints that reset runner registration tokens
- The gitlab-runner exec command is deprecated
- DAST API scans using DAST template is deprecated
- DAST API variables
- DAST report variables deprecation
- KAS Metrics Port in GitLab Helm Chart
- Shimo integration
- Support for periods (.) in Terraform state names might break existing states
- The Phabricator task importer is deprecated
- ZenTao integration
- POST ci/lint API endpoint deprecated
'15.6':
- GitLab Runner registration token in Runner Operator
- Registration tokens and server-side runner arguments in POST /api/v4/runners endpoint
- Registration tokens and server-side runner arguments in gitlab-runner register command
- runnerRegistrationToken parameter for GitLab Runner Helm Chart
- Configuration fields in GitLab Runner Helm Chart
'15.5':
- GraphQL field confidential changed to internal on notes
- vulnerabilityFindingDismiss GraphQL mutation
- File Type variable expansion in .gitlab-ci.yml
'15.4':
- Vulnerability confidence field
- Container Scanning variables that reference Docker
- Non-expiring access tokens
- Starboard directive in the config for the GitLab Agent for Kubernetes
- Toggle behavior of /draft quick action in merge requests
'15.3':
- Atlassian Crowd OmniAuth provider
- CAS OmniAuth provider
- Maximum number of active pipelines per project limit (ci_active_pipelines)
- Redis 5 deprecated
- Security report schemas version 14.x.x
- Use of id field in vulnerabilityFindingDismiss mutation
- Bundled Grafana deprecated
'15.2':
- Remove job_age parameter from POST /jobs/request Runner endpoint
'15.11':
- Deprecate legacy shell escaping and quoting runner shell executor
- GitLab Runner images based on Alpine 3.12, 3.13, 3.14
- 'Geo: Project repository redownload is deprecated'
'15.10':
- DingTalk OmniAuth provider
- Bundled Grafana Helm Chart is deprecated
- Deprecated Consul http metrics
- Environment search query requires at least three characters
- Legacy Gitaly configuration method
- Major bundled Helm Chart updates for the GitLab Helm Chart
- Work items path with global ID at the end of the path is deprecated
'15.1':
- PipelineSecurityReportFinding projectFingerprint GraphQL field
- project.pipeline.securityReportFindings GraphQL query
- Jira DVCS connector for Jira Cloud
- PipelineSecurityReportFinding name GraphQL field
- Vulnerability Report sort by Tool
'15.0':
- GraphQL API legacyMode argument for Runner status
- PostgreSQL 12 deprecated
- Vulnerability Report sort by State
'14.9':
- GitLab self-monitoring project
- Deprecate support for Debian 9
- Background upload for object storage
- GitLab Pages running as daemon
- GraphQL permissions change for Package settings
- Move custom_hooks_dir setting from GitLab Shell to Gitaly
- htpasswd Authentication for the Container Registry
- user_email_lookup_limit API field
- Permissions change for downloading Composer dependencies
'14.8':
- Deprecate legacy Gitaly configuration methods
- CI_BUILD_* predefined variables
- started iteration state
- SAST analyzer consolidation and CI/CD template changes
- Container Network and Host Security
- Dependency Scanning Python 3.9 and 3.6 image deprecation
- Deprecate Geo Admin UI Routes
- Deprecate custom Geo:db:* Rake tasks
- Deprecate feature flag PUSH_RULES_SUPERSEDE_CODE_OWNERS
- Elasticsearch 6.8
- External status check API breaking changes
- GraphQL ID and GlobalID compatibility
- OAuth tokens without expiration
- Optional enforcement of PAT expiration
- Optional enforcement of SSH expiration
- Out-of-the-box SAST support for Java 8
- Querying Usage Trends via the instanceStatisticsMeasurements GraphQL node
- Request profiling
- Required pipeline configurations in Premium tier
- Retire-JS Dependency Scanning tool
- SAST support for .NET 2.1
- Secret Detection configuration variables deprecated
- Secure and Protect analyzer images published in new location
- Secure and Protect analyzer major version update
- Support for gRPC-aware proxy deployed between Gitaly and rest of GitLab
- Test coverage project CI/CD setting
- Vulnerability Check
- projectFingerprint in PipelineSecurityReportFinding GraphQL
- Configurable Gitaly per_repository election strategy
'14.7':
- Monitor performance metrics through Prometheus
- Container scanning schemas below 14.0.0
- Coverage guided fuzzing schemas below 14.0.0
- DAST schemas below 14.0.0
- Dependency scanning schemas below 14.0.0
- Enforced validation of security report schemas
- Godep support in License Compliance
- Logging in GitLab
- Pseudonymizer
- SAST schemas below 14.0.0
- Secret detection schemas below 14.0.0
- Sidekiq metrics and health checks configuration
- Static Site Editor
- Tracing in GitLab
- artifacts:reports:cobertura keyword
'14.6':
- CI/CD job name length limit
- Legacy approval status names from License Compliance API
- type and types keyword in CI/CD configuration
- apiFuzzingCiConfigurationCreate GraphQL mutation
- bundler-audit Dependency Scanning tool
'14.5':
- Package pipelines in API payload is paginated
- Self-managed certificate-based integration with Kubernetes
- GraphQL API Runner status will not return paused
- SaaS certificate-based integration with Kubernetes
- Changing an instance (shared) runner to a project (specific) runner
- Known host required for GitLab Runner SSH executor
- Support for SLES 12 SP2
- Update to the Container Registry group-level API
- Value Stream Analytics filtering calculation change
- Versions on base PackageType
- defaultMergeCommitMessageWithDescription GraphQL API field
- dependency_proxy_for_private_groups feature flag
- pipelines field from the version field
- promote-db command from gitlab-ctl
- promote-to-primary-node command from gitlab-ctl
- openSUSE Leap 15.2 packages
'14.3':
- Audit events for repository push events
- GitLab Serverless
- Legacy database configuration
- OmniAuth Kerberos gem
'14.2':
- Release CLI distributed as a generic package
- Rename Task Runner pod to Toolbox
'14.10':
- Toggle notes confidentiality on APIs
- Dependency Scanning default Java version changed to 17
- Outdated indices of Advanced Search migrations
'14.0':
- Changing merge request approvals with the /approvals API endpoint
- NFS for Git repository storage
- OAuth implicit grant
