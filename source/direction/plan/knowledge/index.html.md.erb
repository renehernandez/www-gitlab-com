---
layout: markdown_page
title: "Group Direction - Knowledge"
description: "Knowledge Group direction page focusing on wiki, pages, and content editor"
canonical_path: "/direction/plan/knowledge/"
---

- TOC
{:toc}

## Knowledge Group

### Introduction

Welcome to the Knowledge Group direction page! Knowledge Group is part of the Plan Stage in GitLab and focuses on three categories, Wiki, Pages, and Content Editor. These categories fall within the [Knowledge Management](https://www.gartner.com/reviews/market/knowledge-management-software) (KM) market as defined by Gartner. The focus of KM is to provide visibility and access to a flow of information across a variety of operations, enabling collaboration that has traditionally existed in silos. Commonly used products in the KM market include Confluence, Notion, ClickUp, Guru, and SharePoint. 

This group direction page, and the associated category direction pages, are a work in progress, and everyone can contribute.

If you have feedback, please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=group%3A%3Aknowledge&first_page_size=20), or create a new issue if you find none fit your criteria. Make sure to tag `@mmacfarlane`, Product Manager for Knowledge Group, so he can read and respond to your comment. Sharing your feedback via issues is the one of the best ways to contribute to our strategy and vision. 

### Strategy and themes

At GitLab, our [vision](https://about.gitlab.com/company/vision/) is to become the AllOps platform, a single application for all innovation. Knowledge Group can be a major contributor to this vision, given its focus on enabling collaboration across users (regardless of their role) and breaking down traditional knowledge silos. An overview of how each Knowledge Group category aligns with this vision is as follows:

Pages: We recognize Pages as a popular feature and one that people really enjoy engaging with as part of the GitLab experience. It exists at the intersection of multiple [stages](https://about.gitlab.com/handbook/product/categories/#devops-stages) of the DevSecOps lifecycle: Create, Plan, Verify, Package, and Release. Our priority for Pages is to provide an experience that guides and supports users to host static assets on the web, regardless of your level of development experience. We can fulfill this strategy by immediately focusing on improving the stability and security of Pages. Afterward, we'll address our most popular issues and compelling blockers for those hosting static content and documentation. Further down the line, we'll validate bigger opportunities around improving the experience of creating and configuring Pages sites and making content management and collaboration easier for non-developer personas.

Content Editor: We've broken down our prioritization of content editor work into two buckets aligned with the theme of improving product usability (a theme which roles up to the vision of an AllOps platform). The first bucket, creating feature parity with the current editing experience, involves [introducing the Rich Text Editor across GitLab](https://gitlab.com/groups/gitlab-org/-/epics/10378). We're confident creating feature parity will improve accessibility and allow users who traditionally feel GitLab is too technical a better opportunity to contribute. After creating feature parity we'll focus on the second bucket, advanced editing and performance stabalization. This bucket will allow us to focus on features such as adding support for text editing features not yet present, aligning text, RTL support, editing footnotes and reference style link. Additionly, this second bucket will allow time to focus on the frontend deserializer (ie preserving source markdown), which provides performance improvements. 

Wiki: For an extensive period of time Wiki has been in [stable maintenance mode](https://docs.gitlab.com/ee/administration/maintenance_mode/). We are actively evaluating future opportunities by first understanding what our existing users are missing and enjoy the most. In FY24 Q2 we are conducting a Problem Validation with an Opportunity Canvas and JTBD framework as the intended output.

### 1 year plan

Pages: Our 1 year plan for Pages can be broken into two distinct theems, stability and improvement. 

Our first theme, stability, focuses on the breakdown of existing bugs and maintenance items, as well as updating documentation related to previous releases. Specific to documentation, we've come to understand that some previous feature rollouts did not include updates to technical documentation, which has caused some confusion to users in utilizing the product. 

Our second theme, improvement, focuses on new feature development. We are acutely aware of four major customer requests as detailed on the [Pages Direction Page](https://about.gitlab.com/direction/plan/knowledge/pages/). We aim to prioritize these items to improve our customer experience, and are thankful to our customers for their patience and feedback as we look to deliver these items.

Content Editor: Our 1 year plan for Content Editor revolves around the theme of GitLab as an AllOps plaftorm. We hope to implement the new Rich Text Editor across GitLab to improve accessibility and efficiency for all users of GitLab. You can follow our progress in the following [General Availability Epic](https://gitlab.com/groups/gitlab-org/-/epics/10378).

Wiki: In FY24Q2 we completed research on the Wiki category in order to develop JTBD. [We've now defined these JTBD](https://gitlab.com/gitlab-org/plan-stage/knowledge-group/-/issues/12) and are utilizing them to inform upon our Wiki strategy. We will look to smoothly transition workload capacity at some point in the feature from another Knowledge category to Wiki category so we can properly meet the demands of our customers.

### What we recently completed

Pages: [Transition GitLab Pages to Object Storage Instead of NFS](https://gitlab.com/groups/gitlab-org/-/epics/3901), [Kubernetes Deployments for GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/39586)

Content Editor: [Display and Edit Markdown Comments in the Content Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/342173), [Insert Table of Contents in the Content Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/366525)

Wiki: [Draw.io/diagrams.net integration with wiki](https://gitlab.com/gitlab-org/gitlab/-/issues/20305/?_gl=1*lfhu8t*_ga*MTU5MDI5ODc5NS4xNjY1NTkyMzcy*_ga_ENFH3X7M5Y*MTY4MTQwNDI2MC40MDYuMS4xNjgxNDA1MzI3LjAuMC4w), [Editing Sidebar in Project or Group Wiki Removes Existing Sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/378028),Relative Links are [Broken On Wiki ASCII Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/377976)

### What we are currently working on

Pages: [Spike on Review Apps for GitLab Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/407776), [Allow Customizing the Artificats Path in GitLab Pages](https://gitlab.com/groups/gitlab-org/-/epics/10126), 
 
Content Editor: [Bringing Content Editor to General Availability](https://gitlab.com/groups/gitlab-org/-/epics/10378)

Wiki: In Milestone 16.1 we refined our Jobs to Be Done (JTBD) for this category to best understand how and what features we should prioritize in future development.

### What's next for Knowledge Group Categories

Pages: As noted under our 1 year vision, we are focusing on the core themes of stability and improvement. Stability focuses on resolution of bugs and maintenance, and improvement focused on resolution of our top 4 customer issues.

Content Editor: Our plan is to continue integrating the Content Editor in the GitLab workflow. We are tracking progress on this initiative via the [Content Editor GA Plan](https://gitlab.com/groups/gitlab-org/-/epics/10378).

Wiki: Wiki was transitioned from Create Stage to Plan Stage at the beginning of FY24. The future direction of Wiki, as a result of this transition, is being evaluated. We've already made good progress in defining JTBD for the category and are looking to hone in on our first priority for development.

### What we are not doing right now

Real-time Collaborative Editing: Ideally, we want to solve the problem of collaborative note taking, be highly approachable for everyone, but also offer the tremendous benefits of storing the content in a portable plain text format that can be cloned, viewed and edited locally (properties of Git). However, we are not actively working on a new architecture that can support this experience.

### Competitive analysis and best-in-class landscape

Knowledge Group Competitive Statement: As state prior, the focus of Knowledge Group is to provide visibility and access to a flow of information across a variety of operations, enabling collaboration that has traditionally existed in silos. Commonly used products in the market include Confluence, Notion, and GitHub. We believe that fundamental improvements in our Pages, Content Editor, and Wiki categories enable use to deliver a more competitive Planning experience against industry leaders.

Pages: We are invested in supporting the process of developing and deploying code from a single place as a convenience for our users. Other providers, such as Netlify, deliver a more comprehensive solution. There are project templates available that offer the use of Netlify for static site CI/CD, while also still taking advantage of GitLab for the SCM, merge requests, issues, and everything else. GitLab offers [configurable redirects](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/24), a well-loved feature of Netlify, made available in gitlab-pages. We are seeing a rise in JAMStack and static site generators partnering in the media. This trend toward API-first, affirms our modernization effort of Pages, reinforcing our cloud native installation maturity plan. GitHub also offers hosting of static sites with GitHub Pages. Key differentiators between the two are that GitHub Pages configuration and deployment is more "automatic" in that it doesn't require you to edit a CI configuration file, and that GitHub Pages has limits placed on bandwidth, builds, and artifact size where GitLab currently does not.

Content Editor: Content Editor aligns as a competitor against both the aforementioned and to-be-mentioned competitors of Pages and Wikis.

Wiki: We currently most closely compete with GitHub Wikis but we would like to compete with Confluence, Notion, Roam Research, and Google Docs. We've heard from customers that managing wikis with tens of thousands of pages can be challenging. And while a full-featured product like Confluence has advanced features and integrations, the GitLab wiki would be a stronger competitor if we fixed some low-hanging fruit related to page title and redirects and improved the functionality of the sidebar to aid navigation.

### Maturity plan

Pages: Pages is currently considered Complete maturity. The next step would be lovable. We believe we are moving toward this state via introduction of the WYSIWYG experience.

Wiki: Wiki is currently considered Viable maturity. Our research indicates to us that we have several key features to develop in order to begin moving the categeory toward Complete. We are continuing to refine our direction to most optimally get us to Complete maturity.

### Target audience

Pages, Content Editor, and Wiki: Our current offering best serves (Sasha, the Software Developer)[https://about.gitlab.com/handbook/product/personas/#sasha-software-developer], but we strive to provide a delightful experience for all personas using Wiki, Pages, and Content Editor. We are investigating the needs of non-developer personas as they are frequent users and prioritizing features to improve their experience. One example of a feature aimed at non-developers is the [WYSWYG content editor](https://docs.gitlab.com/ee/user/project/wiki/#content-editor).  
